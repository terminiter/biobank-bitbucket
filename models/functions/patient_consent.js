var fs = require('fs');
var Patient = require('../schema/patient.js');

exports.upload = function(patientid, image, callback) {
	if(patientid && image && image.name != "")
	{
		var filename = image.name;
		var ftype = filename.substr(filename.lastIndexOf(".")+1);

		Patient.findById(patientid, function(err, patient){
			if(err)
			{
				callback(err, null);
				return;
			}

			if(patient)
			{
				fs.readFile(image.path, function (err, data) {
					if(err)
					{
						callback(err, null);
						return;
					}

					var newPath = process.cwd()+'/static/uploads/patient_consent/'+patient._id+'.'+ftype;
					console.log(newPath);
					var location = '/uploads/patient_consent/'+patient._id+'.'+ftype;

					fs.writeFile(newPath, data, function (err) {
						if(err)
						{
							callback(err, null);
							return;
						}
						edit(patient, location.toString(), ftype.toString(), callback);
					});
				});
			}
		})
	}
};

exports.delete = function(id, callback)
{
	Patient.findById(id, function(err, patient){
		if(patient.consent.location)
			fs.exists(patient.consent.location, function(exists) {
				if(exists)
				{
					fs.unlink(patient.consent.location, function(err){
						remove(patient, callback);
						return
					})
				}
				else
				{
					remove(patient, callback);
					return
				}
			})
		else
		{
			remove(patient, callback);
			return
		}
	});
}

var edit = function(patient, location, type, callback)
{
	Patient.findById(patient._id, function(err, patient){
		patient.consent = {hasImage: true, location: location, type: type}
		patient.save(function(err, patient){
			callback(err, patient)
		})
	});
}

var remove = function(patient, callback)
{
	Patient.findById(patient._id, function(err, patient){
		patient.consent = {hasImage: false, location: "", type: ""}
		patient.save(function(err, patient){
			callback(err, patient)
		})
	});
}