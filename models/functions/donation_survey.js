var Donation = require('../schema/donation.js');
var Survey = {};


Survey.add = function(donation_id, data, callback)
{
	Donation.findById(donation_id, function(err, donation){
		if(donation && data.survey && data.created && data.results) 
		{
			var survey = {};
			survey.title = data.survey.title;
			survey.survey = data.survey._id;
			survey.created = data.created;

			var results = [];
			for(var i = 0; i<data.results.length; i++)
			{
				var result = data.results[i];
				if(result.type && result.name && result.value)
				{
					if(result.value != "")
					{
						var obj = surveyResult(result.type,result.name,result.value)
						results.push(obj);
					}
				}

				if(result.advanced)
				{
					var name = "Adv: " + result.name;
					results = advAndSpecifiy(result.advanced, name, results);

				}

				if(result.specify)
				{
					var name = "Spec: " + result.name;
					results = advAndSpecifiy(result.specify, name, results);
				}
			}

			survey.results = results;

			donation.surveys.push(survey);

			donation.save(function(err) {
				if(err) throw err;
				Donation.findById(donation,callback);
			})
		}
		else
		{
			callback(null);
		}
	});
}

var advAndSpecifiy = function(arr, topname, globalarr)
{
	for(var j = 0; j<arr.length; j++)
	{
		var result = arr[j];
		

		if(result)
			if(result.type && result.name && result.value)
			{
				var name = topname + " - " + result.name;
				var objAd = surveyResult(result.type,name,result.value)
				globalarr.push(objAd);
			}
	}

	return globalarr;
}

var surveyResult = function(type, key, value)
{
	var obj = {type: type, key: key, value: value};
	if(type == "mc" || type == "list")
	{
		if(value.length > 0 && value.join)
			obj.value = value.join("__");
	}

	return obj;
}

Survey.delete = function(date, id, survey_index, survey_id, callback)
{
	var dateStr = new Date().toDateString().split(" ").slice(1).join("-");
	if(dateStr == date)
		Donation.findOne({"_id":id, "surveys._id":survey_id}, function(err, object) {
			if(object && object.surveys[survey_index])
			{
				object.surveys.splice(survey_index,1);
				object.save(function(err) {
					if(err) throw err;
					Donation.findById(object,callback);
				})
			}
			else
			{
				callback("Not Here");
			}

		});
	else
		callback("Not Here");
}

Survey.Result = {
	edit: function()
	{

	},
	delete: function(date, id, survey_index, survey_id, result_index, result_id, callback)
	{
		var dateStr = new Date().toDateString().split(" ").slice(1).join("-");
		if(dateStr == date)
			Donation.findOne({"_id":id, "surveys._id":survey_id, "surveys.results._id":result_id}, function(err, object) {
				if(object && object.surveys[survey_index] && object.surveys[survey_index].results[result_index])
				{
					object.surveys[survey_index].results.splice(result_index,1);
					object.save(function(err) {
						if(err) throw err;
						Donation.findById(object,callback);
					})
				}
				else
				{
					callback("Not Here");
				}

			});
		else
			callback("Not Here");
	},
	update: function(id, survey_index, survey_id, result_index, result_id, data, callback)
	{
		Donation.findOne({"_id":id, "surveys._id":survey_id, "surveys.results._id":result_id}, function(err, object) {
			if(object && object.surveys[survey_index] && object.surveys[survey_index].results[result_index])
			{
				console.log();
				var result = object.surveys[survey_index].results[result_index];
				result.type = data.type;
				result.name = data.name;
				result.value = data.value;
				object.surveys[survey_index].results[result_index] = result;
				console.log(object.surveys[survey_index].results[result_index]);
				object.save(function(err) {
					if(err) throw err;
					Donation.findById(object,callback);
				})
			}
			else
			{
				callback("Not Here");
			}
		});
	},
	create: function(id, survey_index, survey_id, data, callback)
	{
		Donation.findOne({"_id":id, "surveys._id":survey_id}, function(err, object) {
			if(object && object.surveys[survey_index] && object.surveys[survey_index].results)
			{
				object.surveys[survey_index].results.push(surveyResult(data.type, data.name, data.value));
				object.save(function(err) {
					if(err) throw err;
					Donation.findById(object,callback);
				})
			}
			else
			{
				callback("Not Here");
			}
		});
	},
	find: function(id, survey_index, survey_id, result_index, result_id, callback)
	{
		Donation.findOne({"_id":id, "surveys._id":survey_id,  "surveys.results._id":result_id}, function(err, object) {
			if(object && object.surveys[survey_index] && object.surveys[survey_index].results)
			{
				var result = object.surveys[survey_index].results[result_index];
				var obj = {result: result, index:result_index};
				callback(null, obj);
			}
			else
			{
				callback("Not Here");
			}
		});
	},
}

module.exports = Survey;