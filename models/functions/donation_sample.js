var Freezer = require('../functions/freezer.js');
var Donation = require('../schema/donation.js');
var Sample = require('../../extensions/crud_functions.js')(Donation.Sample);

Sample.find = function(sample_id, callback)
{
	Donation.find({"samples._id":sample_id})
		.exec(function(err, donation) {
			if(!donation || err || !donation.samples || donation.samples.length == 0)
			{
				callback("No Donation", null);
				return;
			}

			for(var i = 0; i<donation.samples.length; i++)
			{
				var coup = donation.samples[i];

				if(coup._id == coupon_id)
				{
					donation.samples = [coup];
					callback(null, donation);
					return;
				}
			}

			callback("No Sample", null);
		})
}
Sample.create = function(donation, locationstr, type, freezer, callback)
{
	var sample = new Sample.tmp(type, freezer);
	var arr = locationstr.split("-");
	sample.donation = donation;
	sample.location.freezer = freezer;
	sample.location.shelf = arr[0];
	sample.location.rack = arr[1];
	sample.location.drawer = arr[2];
	sample.location.box = arr[3];
	sample.location.box_location.x = arr[4];
	sample.location.box_location.y = arr[5];
	Donation.Sample.create(sample, function(err, saved_sample) {
		if(saved_sample != null)
			callback(err, saved_sample)
		else
			callback(err, null);
	})
}
Sample.tmp = function(type, freezer)
{
	return {
		type: type,
		location: {
			freezer: freezer,
			shelf: 0,
			rack: 0,
			draw: 0,
			box: 0,
			box_location: {x:0, y:0}
		}
	}
}

Sample.update = function(id, data, donation_id, callback)
{
	Donation.findOne({"samples._id":id}, function(err,donation) {
		findSampleIndex(id, donation._id, function(index, sample){
			if(data && data.type && index >= 0 && index < donation.samples.length)
			{
				donation.samples[index].type = data.type;
				sample.type = data.type;
				sample.save(function(err) {
					donation.save(function(err) {
						if(err) throw err;
						Donation.findById(donation,callback);
					})
				});
			}
			else
			{
				callback("Err", null)
			}
		})
	})
}
Sample.delete = function(id, donation_id, callback)
{
	Donation.findOne({"samples._id":id}, function(err,donation) {
		findSampleIndex(id, donation._id, function(index){
			if(index >= 0 && index < donation.samples.length)
			{
				console.log(donation._id);
				Donation.Sample.findByIdAndRemove(id, function(err) {
					console.log(err);
					donation.samples.splice(index, 1);
					donation.save(function(err) {
						if(err) throw err;
						Donation.findById(donation,callback);
					})
				})
			}
			else
			{
				callback("Err", null)
			}
		})
	})
}

var findSampleIndex = function(sample_id, donation_id, callback)
{
	Donation.findById(donation_id, function(err, donation){
		if(donation && donation.samples && donation.samples.length > 0)
		{
			for(var i = 0; i<donation.samples.length; i++)
			{	
				if(donation.samples[i] && donation.samples[i]._id == sample_id)
				{
					Donation.Sample.findById(sample_id, function(err, sample){
						callback(i,sample);
					})
					return
				}
			}
			callback(-1);
		}
		else
		{
			callback(-2);
		}
	})
}

Sample.getNumType = function(typeId, callback)
{
	Donation.Sample.count({type:typeId}, function(err, count){
		if(err) 
			count = 1;

		callback(count);
	})
}

module.exports = Sample;