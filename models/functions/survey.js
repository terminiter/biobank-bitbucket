var Questions = require('./survey_question.js');
var Survey = require('../schema/survey.js');
exports = require('../../extensions/crud_functions.js')(Survey);

exports.getAll = function(callback)
{
	Survey.find({}, function(err, data){
		if (err) throw err;
		

		for(var i = 0; i<data.length; i++)
		{
			survey = data[i];
			var advanced = 0;
			for(var q = 0; q<survey.questions.length; q++)
			{
				advanced += survey.questions[q].advanced_questions.length;
			}
			data[i].advanced = advanced;
		}

		callback(data);

	})
}

exports.getAllSmall = function(callback)
{
	Survey.find({},'title description', function(err, data){
		callback(data);
	});
}
module.exports = exports;