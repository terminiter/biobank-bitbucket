var Donation = require('../schema/donation.js');

exports.key = function(key, callback)
{
	Donation.aggregate()
	.match({})
	.project({
		"key":"$surveys.results.key",
		"k":1
	})
	.unwind('key')
	.unwind('key')
	.match({"key":{$regex: new RegExp(key, "i")}})
	.sort({"key":1})
	.group({
		"_id":'$k',
		"keys":{$addToSet:"$key"}
	})
	.exec(function(err, results){
		if(err) throw err;
		var result = [];
		if(results[0] && results[0].keys)
			result = results[0].keys

		callback(err, result);
	});
}

exports.value = function(value, callback)
{
	Donation.aggregate()
	.match({})
	.project({
		"value":"$surveys.results.value",
		"k":1
	})
	.unwind('value')
	.unwind('value')
	.match({"value":{$regex: new RegExp(value, "i")}})
	.sort({"value":1})
	.group({
		"_id":'$k',
		"values":{$addToSet:"$value"}
	})
	.exec(function(err, results){
		if(err) throw err;
		var result = [];
		if(results[0] && results[0].values)
			result = results[0].values

		callback(err, result);
	});
}


exports.keyValue = function(key,value, callback)
{
	Donation.aggregate()
	.match({"surveys.results.value":{$regex: new RegExp(value, "i")}, "surveys.results.key":{$regex: new RegExp(key, "i")}})
	.project({
		"surveys":"$surveys"
	})
	.unwind("surveys")
	.project({
		"results":"$surveys.results"
	})
	.unwind("results")
	.project({
		"key":"$results.key",
		"value":"$results.value",
		"v":1
	})
	.match({"value":{$regex: new RegExp(value, "i")}, "key":{$regex: new RegExp(key, "i")}})
	.group({
		"_id":'$v',
		"values":{$addToSet:"$value"}
	})
	.exec(function(err, results){
		if(err) throw err;
		var result = [];
		if(results[0] && results[0].values)
			result = results[0].values

		var newresult = [];
		for(var i = 0; i<result.length; i++)
		{
			if(result[i].indexOf("__") != -1)
			{
				var arr = result[i].split("__");
				for(var j = 0; j<arr.length; j++)
				{
					if(newresult.indexOf(arr[j]) == "-1")
						newresult.push(arr[j]);
				}
			}
			else
				if(newresult.indexOf(result[i]) == "-1")
					newresult.push(result[i])
		}
		callback(err, newresult);
	});
}


exports.find = function(query, callback)
{
	if(query && query.length)
	{
		var obj = {$and: []};
		for(var i = 0; i<query.length;i++)
		{

			obj["$and"].push({"surveys.results.key": new RegExp(query[i].key, "i"), "surveys.results.value": new RegExp(query[i].value, "i")});
		}

		console.log(obj["$and"]);

		Donation.find(obj)
		.exec(function(err, donations)
		{
			Donation.populate(donations,'patient', function(err, donations){
				callback(err,donations);
			});
		})
	}
	else
	{
		callback(null, []);
	}
}