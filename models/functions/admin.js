var bcrypt = require('bcrypt');
var Admin = require('../schema/admin.js');
exports = require('../../extensions/crud_functions.js')(Admin);

exports.create_console = function(name, password) {
	bcrypt.hash(password, 8, function(err, hash) {
		if (err) throw err;
		Admin.create({name: name, password: hash}, function(err, admin) {
			console.log("Saved!");
			console.log("Username:" +admin.username + ' | password: '+password);
		});
	});
};

exports.login = function(username, password, callback) {
	Admin.findOne({username: username}, function(err, admin) {
		if (err) return callback(false, null);
		if (!admin) return callback(false, null);

		// password is hashed
		bcrypt.compare(password, admin.password, function(err, match) {
			if (err || !match)
				return callback(false, null);

			callback(true, admin);
		});
	});
}

exports.create = function(data, callback)
{
	var s = new Admin(data);

	bcrypt.hash(data.password, 8, function(err, hash) {
		s.password = hash;
		s.save(function(err) {
			if(err) throw err;
			Admin.findById(s,callback);
		})
	});
}
exports.update = function(id, data, callback)
{
	if(data.password && data.password != "")
	{
		bcrypt.hash(data.password, 8, function(err, hash) {
			if (err) throw err;
			data.password = hash;
			Admin.findByIdAndUpdate(id, data, callback)
		});
	}
	else
	{
		Admin.findByIdAndUpdate(id, data, callback)
	}
	
}
module.exports = exports;