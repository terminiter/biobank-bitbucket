var Donation = require('../schema/donation.js');

var DataEntry = {};

DataEntry.create = function(id, data, callback)
{
	Donation.findById(id, function(err, donation){
		if(donation)
		{
			var obj = {type:"",location:"", description:""};

			obj.type = data.type+"";
			obj.location = data.location+"";
			obj.description = data.description+"";

			donation.media.push(obj);

			console.log(donation);

			donation.save(function(err) {
				if(err) throw err;
				Donation.findById(donation,callback);
			})
		}
		else
		{
			callback(null, null);
		}
	});
}
DataEntry.update = function(id, index, entryid, data, callback)
{
	Donation.findById(id, function(err, donation){
		if(donation)
		{

			if(donation.media[index] != null && entryid != null)
			{

				var obj = donation.media[index];

				obj.type = data.type+"";
				obj.location = data.location+"";
				obj.description = data.description+"";
				obj._id = entryid;

				donation.media[index] = obj;

				console.log(donation);
				donation.save(function(err) {
					if(err) throw err;
					Donation.findById(donation,callback);
				})
			}
		}
		else
		{
			callback(null, null);
		}
	});
}

DataEntry.delete = function(id, index,date, callback)
{
	var dateStr = new Date().toDateString().split(" ").slice(1).join("-");

	if(dateStr == date)
		Donation.findById(id, function(err, donation){
			if(donation)
			{
				if(donation.media[index] != null)
				{
					donation.media.splice(index, 1);

					donation.save(function(err) {
						if(err) throw err;
						Donation.findById(donation,callback);
					})
				}
			}
			else
			{
				callback(null, null);
			}
		});
	else
		callback(null);
}

DataEntry.get = function(id, index, entryId, callback)
{
	Donation.findById(id, function(err, donation){
		if(donation)
		{

			if(donation.media[index] != null)
			{
				if(donation.media[index]._id == entryId)
					callback(null, donation.media[index]);
				else
					callback(null, null);	
			}
			else
				callback(null, null);
		}
		else
		{
			callback(null, null);
		}
	});
}

module.exports = DataEntry;