var Freezer = require('../schema/freezer.js');
var Donation = require('../schema/donation.js');
var async = require('async');

exports.getLocation = function(id, callback)
{
	var location = getLocationObject(id, 0, 0, 0, 0, 0, 0);
	getFreezer(id, function(freezer) {
		location.shelf = random(freezer.shelves.length);
		location.rack = random(freezer.shelves[location.shelf].num_racks);
		location.drawer = random(freezer.shelves[location.shelf].num_drawers);
		location.box = random(freezer.shelves[location.shelf].num_boxes);
		location.box_location.x = random(freezer.shelves[location.shelf].box_dim.value);
	 	location.box_location.y = random(freezer.shelves[location.shelf].box_dim.value);
	 	callback(location);

	 })
}

exports.findFreeLoactions = function(freezer_id, amount, callback)
{
	var openLoc = [];
	getFreezer(freezer_id, function(freezer) {
		getCurrentLocations(freezer._id, function(locations) {
			for(var s = 0; s<freezer.shelves.length; s++)
			{

				var shelf = freezer.shelves[s];
				for(var r = 0; r<shelf.num_racks; r++)
					for(var d = 0; d<shelf.num_drawers; d++)
						for(var b = 0; b<shelf.num_boxes; b++)
							for(var x = 0; x<shelf.box_dim.value; x++)
								for(var y = 0; y<shelf.box_dim.value; y++)
								{
									var str = s+"-"+r+"-"+d+"-"+b+"-"+x+"-"+y
									if(locations.indexOf(str) == -1)
									{
										if(amount && openLoc.length < amount)
										{
											openLoc.push(str);
										}
										else
										{
											callback(openLoc);
											return;
										}
									}

								}
			}
		})
	})
}

exports.findAllLocations = function(freezer_id, callback)
{
	getCurrentLocations(freezer_id, function(location){
		callback(location);
	});
}

exports.findSamples = function(freezer_id,shelf,rack,drawer,box,x,y,callback)
{
	var location = getLocationObject(freezer_id,shelf,rack,drawer,box,x,y);
	Donation.Sample.find(location)
	.sort({"location.shelf": 1,"location.rack": 1,"location.drawer": 1,"location.box": 1,"location.box_location.x":1, "location.box_location.y":1})
	.exec(function(err, samples){
		Donation.Sample.populate(samples, 'type', function(err,samples) {
			callback(samples);
		})
	})
}

exports.findSpace = function(freezer_id,shelf,rack,drawer,box,x,y,callback)
{
	getFreezer(freezer_id, function(freezer){
		if(freezer)
		{
			var location = getLocationObject(freezer_id,shelf,rack,drawer,box,x,y);
			Donation.Sample.count(location)
			.exec(function(err, samples){
				getSpace(freezer, location, callback);
			})
		}
		else
		{
			callback("Invalid Freezer");
		}
	})
}

exports.findAddress = function(freezer_id, callback)
{
	getFreezer(freezer_id, function(freezer){
		if(freezer)
		{
			var array = [];
			for(var s = 0; s < freezer.shelves.length; s++)
			{
				var shelf = freezer.shelves[s];
				var sArr = [];
				for(var r = 0; r<shelf.num_racks; r++)
				{
					var rArr = [];
					for(var d = 0; d<shelf.num_drawers; d++)
					{
						var dArr = [];
						for(var b = 0; b<shelf.num_boxes; b++)
						{
							var bArr = [];
							for(var x=0; x<shelf.box_dim.value; x++)
							{
								var xbArr = []
								for(var y=0; y<shelf.box_dim.value; y++)
								{
									xbArr.push({x:x,y:y});
								}
								bArr.push(xbArr);
							}
							dArr.push(bArr);
						}
						rArr.push(dArr);
					}
					sArr.push(rArr);
				}
				array.push(sArr);
			}
			callback(array);
		}
		else
		{
			callback("Invalid Freezer");
		}
	})
}

exports.findAllSpace = function(freezer_id,shelf,rack,drawer,box,x,y,callback)
{
	getFreezer(freezer_id, function(freezer){
		if(freezer)
		{
			var location = getLocationObject(freezer_id,shelf,rack,drawer,box,x,y);
			getSpaces(freezer, location, function(spaces) {
				callback(spaces)
			})
		}
		else
		{
			callback("Invalid Freezer");
		}
	})
}

var getCurrentLocations = function(freezer_id, callback)
{
	var location = getLocationObject(freezer_id);
	Donation.Sample.find(location, 'location')
	.sort({"location.shelf": 1,"location.rack": 1,"location.drawer": 1,"location.box": 1,"location.box_location.x":1, "location.box_location.y":1})
	.exec(function(err, samples){
		changeSamples(samples, callback)
	})
}

var changeSamples = function(samples, callback)
{
	var samp = []
	for(var i = 0; i<samples.length; i++)
	{
		var sample = samples[i].location;
		var str = sample.shelf+"-"+sample.rack+"-"+sample.drawer+"-"+sample.box+"-"+sample.box_location.x+"-"+sample.box_location.y;
		samp.push(str);
	}
	callback(samp);
}

var getFreezer = function (id, callback) {
	Freezer.findById(id, function(err, freezer){
		callback(freezer);
	})
}

var getLocationObject = function (freezer_id,shelf,rack,drawer,box,x,y) {

	var obj = {
		"location.freezer":freezer_id,
		"location.shelf":shelf,
		"location.rack":rack,
		"location.drawer":drawer,
		"location.box":box,
		"location.box_location.x":x,
		"location.box_location.y":y
	}

	if(freezer_id == null)
		return null;

	if(shelf == null)
	{
		delete obj["location.shelf"];
		delete obj["location.rack"];
		delete obj["location.drawer"];
		delete obj["location.box"];
		delete obj["location.box_location.x"];
		delete obj["location.box_location.y"];
	}
	if(rack == null)
	{
		delete obj["location.rack"];
		delete obj["location.drawer"];
		delete obj["location.box"];
		delete obj["location.box_location.x"];
		delete obj["location.box_location.y"];
	}

	if(drawer == null)
	{
		delete obj["location.drawer"];
		delete obj["location.box"];
		delete obj["location.box_location.x"];
		delete obj["location.box_location.y"];
	}

	if(box == null)
	{
		delete obj["location.box"];
		delete obj["location.box_location.x"];
		delete obj["location.box_location.y"];
	}
	if(x == null)
	{
		delete obj["location.box_location.x"];
		delete obj["location.box_location.y"];
	}
	if(y == null)
	{
		delete obj["location.box_location.y"];
	}

	obj["donation"] = {"$ne":null}
	return obj;
}

var getSpace = function(freezer, location, callback)
{
	Donation.Sample.count(location)
	.exec(function(err, samples){
		var space = {samples: samples, free:0, available:0};
		space.free = getTotalSpace(freezer, location) - samples;
		if(space.free > 0)
			space.available = (1 - Math.floor((space.samples/space.free)*10000)/10000);
		if(space.available < 0)
			space.available = 0;
		callback(space);
	})
}

var getTotalSpace = function(freezer, location)
{
	var space = 0;
	if(freezer != null && freezer.shelves)
	{
		if(location["location.shelf"] == null)
		{
			for(var i = 0; i<freezer.shelves.length; i++)
			{
				var shelf = freezer.shelves[i];
				var shelf_amount = (shelf.num_racks*shelf.num_drawers*shelf.num_boxes*shelf.box_dim.value*shelf.box_dim.value);
				if(shelf)
					space += shelf_amount;
			}
		}
		else if(freezer.shelves[location["location.shelf"]])
		{
			var shelf = freezer.shelves[location["location.shelf"]];
			var shelf_amount = (shelf.num_racks*shelf.num_drawers*shelf.num_boxes*shelf.box_dim.value*shelf.box_dim.value);
			
			if(location["location.rack"])
				shelf_amount = shelf_amount/shelf.num_racks
			
			if(location["location.drawer"])
				shelf_amount = shelf_amount/shelf.num_drawers
			if(location["location.box"])
				shelf_amount = shelf_amount/shelf.num_boxes
			if(location["location.box_location.x"])
				shelf_amount = shelf_amount/shelf.box_dim.value
			if(location["location.box_location.y"])
				shelf_amount = shelf_amount/shelf.box_dim.value
				
			space = shelf_amount;

		}
	}

	return space;
}

var getSpaces = function(freezer, location, callback)
{
	var spaces = []
	if(location["location.shelf"] == null)
	{
		makeSpaceArray(freezer, freezer.shelves.length, location, "location.shelf", callback);
		return;
	}
	else if(freezer.shelves[location["location.shelf"]])
	{
		var checkLoc = [
			{str: "location.rack",number: freezer.shelves[location["location.shelf"]].num_racks},
			{str: "location.drawer",number: freezer.shelves[location["location.shelf"]].num_drawers},
			{str: "location.box",number: freezer.shelves[location["location.shelf"]].num_boxes},
			{str: "location.box_location.x",number: freezer.shelves[location["location.shelf"]].box_dim.value},
			{str: "location.box_location.y",number: freezer.shelves[location["location.shelf"]].box_dim.value}
		]

		for(var i = 0; i<checkLoc.length; i++)
		{
			var check = checkLoc[i];
			if(location[check.str]==null)
			{
				makeSpaceArray(freezer, check.number, location, check.str, callback);
				return;
			}
		}
		callback(spaces)
		return;
	}
	else
	{
		callback(spaces)
	}
}

var makeSpaceArray = function(freezer, number, location, locationStr, callback)
{
	var spaces = new Array(number);
	async.times(number, function(n, next){
		var location_2 = location;
		location_2[locationStr] = n;
		getSpace(freezer, location_2, function(space) {
			spaces[n] = space;
			next()
		})
	},function(err){
		callback(spaces);
	})
}

var random = function(max)
{
	return Math.floor(Math.random()*max);
}