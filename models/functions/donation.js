var Patient = require('../schema/patient.js');
var Donation = require('../schema/donation.js');
var async = require('async');
var exports = require('../../extensions/crud_functions.js')(Donation);
var functions = require('../../extensions/crud_functions.js')(Donation);
var Freezer = require('../functions/freezer.js');
var functions = require('../../extensions/crud_functions.js')(Donation);
var Sample = require('../../extensions/crud_functions.js')(Donation.Sample);

exports.Survey = require('./donation_survey.js');
exports.Sample = require('./donation_sample.js');
exports.Search = require('./donation_search.js');
exports.DataEntry = require('./donation_dataentry.js');

exports.getOne = function(id, callback)
{
	Donation.findById(id, function(err, donation) {
		Donation.populate(donation,'patient', function(err, donation){
			Donation.populate(donation,'samples.type', function(err, donation){
				Donation.populate(donation,'samples.location.freezer', function(err, donation){
					exports.createLabel(donation, function(donation){
						callback(err, donation);
					})
				})
			})
		})
	})
}

exports.getMutiple = function(donations, callback)
{
	var don = [];

	async.each(donations, function(donation, next) {
		exports.getOne(donation._id, function(err, dUpdated) {
			don.push(dUpdated);
			next();
		})
	}, function(err) {
		callback(don);
	})
}

exports.findByPatient = function(patient_id, callback)
{
	Donation.find({"patient":patient_id}, function(err, donations) {
		if(donations)
			Donation.populate(donations,'patient', function(err, donations) {
				Patient.findById(patient_id, function(err, patient) {
					callback(err, donations, patient);
				})
			});
		else
			callback(err, null)
	})
}

exports.getLabels = function(id, callback)
{
	exports.getOne(id, function(err, donation){
		var data = [];
		for(var i = 0; i<donation.samples.length; i++)
		{
			data.push({label: donation.samples[i].label});
		}
		callback(err, donation, data);
	})
}
exports.getAll = function(callback)
{
	functions.getAll(function(donations) {
		Donation.populate(donations,'patient', function(err, donations){
			callback(donations);
		})
	})
}
exports.create = function(data, callback)
{
	functions.create(data, function(err, donation) {
		tmp_samples = getNewSampleArray(data.samples);
		if(tmp_samples.length > 0)
		{
			var saved_samples = [];
			exports.addSamples(donation, tmp_samples,data.freezer, false, callback)
		}
		else
			callback(null, donation)

	})
}
exports.delete = function(date, id, callback)
{

	var dateStr = new Date().toDateString().split(" ").slice(1).join("-");
	if(dateStr == date)
		Donation.findById(id, function(err, object) {
			async.each(object.samples, function(sample, cb){
				Donation.Sample.findByIdAndRemove(sample._id, function(err) {
					if(err) throw err;

					cb();
				})
			}, function(err) {
				Donation.findByIdAndRemove(object._id, function(err) {
					if(err)
						callback(err);
					else
						callback("OK");
				})
			})
		});
	else
		callback("Not Here");
}

exports.createLabel = function(donation, callback)
{
	Donation.populate(donation, "samples.type", function(err, donation) 
	{
		Donation.populate(donation, "samples.location.freezer", function(err, donation) 
		{
			async.map(donation.samples, function(sample, next) {
				sample.label = ""+(sample.location.freezer.freezerId+1);
				sample.label += ""+(sample.location.shelf+1);
				sample.label += ""+(sample.location.rack+1);
				sample.label += ""+(sample.location.drawer+1);
				sample.label += ""+(sample.location.box+1);
				sample.label += ""+String.fromCharCode(65+sample.location.box_location.x);
				sample.label += ""+(sample.location.box_location.y+1);
				sample.label += ""+sample.type.name.substr(0,1).toUpperCase();
				next(err, sample)
			}, function(err, samples) {
				donation.samples = samples;
				callback(donation);
			})
		});
	});
}

var getNewSampleArray = function(samples_data)
{
	var samples = [];
	for(var i = 0; i<samples_data.length; i++)
	{
		var type = samples_data[i].type;
		var amount = samples_data[i].number;
		for(var j = 0; j<amount; j++)
		{
			samples.push(type);
		}
	}
	return samples;
}

exports.addSamples = function(donation, typeArr, freezer, append, callback)
{
	Freezer.locations.findFreeLoactions(freezer, typeArr.length, function(locations) {
		async.times(locations.length, function(n,next) {
			exports.Sample.create(donation, locations[n], typeArr[n], freezer, next)
		},
		function(err, samples) {
			if(append)
				donation.samples = samples.concat(donation.samples);
			else
				donation.samples = samples;
			donation.save(function(err) {
				if(err) throw err;
				Donation.findById(donation,callback);
			})
		});
	})
}

exports.deleteByPatient = function(date, patient_id, callback)
{
	Donation.find({"patient":patient_id}, function(err, donations) {
		async.each(donations, function(donation, next) {
			exports.delete(date, donation._id, function(out) {
				next();
			})
		}, function(err){
			if(err)
				callback(err);
			else
				callback("OK");
		})
	})
}

module.exports = exports;