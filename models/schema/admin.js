var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var adminSchema = new Schema({
	username: {type: String, unique:true},
	password: String,
	email: String,
	department: String,
	access: {type: Number, default: 0},
	name: String
});

// create a slug from the store name
adminSchema.pre('save', function(next) {
  var username = this.name.toLowerCase().replace(/[ ]/g, '-').replace(/[^a-zA-Z0-9-]/g, '');
  this.username = username;

  next();
});

// build and expose model
module.exports = mongoose.model('Admin', adminSchema);