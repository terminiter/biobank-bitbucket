var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var connection = require('../../extensions/database.js');
var autoIncrement = require('mongoose-auto-increment');
autoIncrement.initialize(connection);

var freezer = new Schema({
	name: String,
	freezerId: Number,
	shelves: 
	[
		{
			num_racks: Number,
			num_drawers: Number,
			num_boxes: Number,
			tube_size: {type: Schema.Types.ObjectId, ref: 'TubeSize'},
			box_dim:
				{
					name:String,
					value: Number,
				}
		}
	]
});

// build and expose model
freezer.plugin(autoIncrement.plugin, { model: 'Freezer', field: 'freezerId' });
module.exports = mongoose.model('Freezer', freezer);