var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var advancedQuestionsSchema = new Schema({
	name: String,
	type: {type:String, enum: ['txt','dd','mc','list']},
	specify:{type: Boolean, default:false},
	answers: 
	[
		{
			name:String,
			specify:{type: Boolean, default:false},
		}
	],
})

var questionSchema = new Schema({
	name: String,
	type: {type:String, enum: ['txt','dd','mc','list']},
	advancedSpecify: {type: Boolean, default:false},
	specify:{type: Boolean, default:false},
	answers: 
	[
		{
			name:String,
			specify:{type: Boolean, default:false},
		}
	],
	advanced_questions: [advancedQuestionsSchema],
})

var surveySchema = new Schema({
	title: String,
	description: String,
	questions: [questionSchema],
	status: {type: String, default: "Incomplete"}
});

// build and expose model
var model = mongoose.model('Survey', surveySchema);
model.Question = mongoose.model('Question', questionSchema);
model.Advanced_Question = mongoose.model('AdvancedQuestion', advancedQuestionsSchema);

module.exports = model;