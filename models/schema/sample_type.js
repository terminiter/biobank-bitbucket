var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var sampleTypeSchema = new Schema({
	name: String,
	color: {type: String, default: "FFFFFF"},
	count: Number
});

// build and expose model
module.exports = mongoose.model('SampleType', sampleTypeSchema);