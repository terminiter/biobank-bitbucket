var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var sampleSchema = new Schema({
	donation: {type: Schema.Types.ObjectId, ref: 'Donation'},
	type: {type: Schema.Types.ObjectId, ref: 'SampleType'},
	location: {
		freezer: {type: Schema.Types.ObjectId, ref: 'Freezer'},
		shelf: Number,
		rack: Number,
		drawer: Number,
		box: Number,
		box_location:{x:Number, y:Number},
	}
});
sampleSchema.index({
	"location.freezer": 1, 
	"location.shelf": 1, 
	"location.rack": 1, 
	"location.drawer": 1, 
	"location.box": 1, 
	"location.box_location.x": 1, 
	"location.box_location.y": 1
}, {unique: true});

var donationSchema = new Schema({
	patient: {type: Schema.Types.ObjectId, ref: 'Patient'},
	date: {type: Date, default: Date.now},
	surveys: 
	[
		{
			title: String,
			survey: {type: Schema.Types.ObjectId, ref: 'Survey'},
			created: {type: Date, default: Date.now},
			results:
			[
				{
					type: {type:String, default: "txt"},
					key: String,
					value: String
				}
			]
		}
	],
	media: [
		{
			type: {type:String, default: ""},
			location: {type:String, default: ""},
			description:{type:String, default: ""},
		}
	],
	samples: [sampleSchema],
});
donationSchema.index({
	"samples.location.freezer": 1, 
	"samples.location.shelf": 1, 
	"samples.location.rack": 1, 
	"samples.location.drawer": 1, 
	"samples.location.box": 1, 
	"samples.location.box_location.x": 1, 
	"samples.location.box_location.y": 1
}, {unique: true});

// build and expose model
var model = mongoose.model('Donation', donationSchema);
model.Sample = mongoose.model('Sample', sampleSchema);
module.exports = model;