var express = require('express');
var cookieSession = require('cookie-session');
var bodyParser = require('body-parser');
var multer = require('multer');

routes = require('./routes');
app = express();
// configure express

app.set('port', process.env.PORT || 3000);
app.set('view engine', 'jade');
app.set('views', __dirname + '/views');
app.set('view options', {layout: false});
app.use(multer({dest: './image_tmp/'}));
app.use(express.static(__dirname + '/static'));
app.use(cookieSession({keys: 
	["4pK3VEhF2I",
	"dnSJpACiNB",
	"Tn4YSzIOuH",
	"9VvTchLznd",
	"gtTOAydkU8",
	"8Yf4PXQK2k",
	"PDsV9FUYD0",
	"kgMIZLSHiu",
	"vZV6QZnRmT",
	"QAOI4W05vB"]}))


app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

app.use(function(req, res, next){
    res.locals = require('./extensions/locals.js');
    next();
});
//Connect to MongoDB
app.set('connection', require('./extensions/database.js'));
require('./extensions/jade.js');

require('./routing.js');
// listen for incoming connections
app.listen(app.get('port'));

console.log("BioBank Server - Created by Byte It Solutions (BiTs)");
console.log("Server Started on port: "+app.get('port'));


