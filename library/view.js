var navbar = require("./navbar.js");

exports.get = function(req, page)
{
	var view = {};
	var type = req.query.t ? req.query.t : "info";
	view.message = {message:req.query.m, type:type};
	view.navbar = navbar.get(req.session.type);
	view.user = req.session.user;
	return view;
};