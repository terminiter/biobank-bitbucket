module.exports =
	[
		{
			name: "Dashboard",
			location:"/", 
			icon:"dashboard", 
			level2: []
		},
		{
			name: "Patients",
			location:"#", 
			icon:"users", 
			level2: 
			[
				{
					name: "Lookup",
					location:"/patients", 
					icon:"search", 
				},
				{
					name: "Add Patient",
					location:"/patient/create", 
					icon:"plus", 
				},
			]
		},
		{
			name: "Samples",
			location:"/samples", 
			icon:"eyedropper", 
			level2: []
		},
		{
			name: "Sample Lookup",
			location:"/sample/lookup", 
			icon:"search", 
			level2: []
		},
		{
			name: "Freezers",
			location:"#", 
			icon:"list-alt", 
			level2: [
				{
					name: "Add Freezer",
					location:"/freezer/create",
					icon:"plus"
				},
				{
					name: "View Freezer",
					location:"/freezers",
					icon:"eye"
				},
			]
		},
		{
			name: "Settings",
			location:"/link", 
			icon:"wrench", 
			level2: 
			[
				{
					name: "Surveys",
					location:"/settings/surveys", 
					icon:"clipboard", 
				},
				{
					name: "Sample Types",
					location:"/settings/samples", 
					icon:"paint-brush", 
				},
				{
					name:"Tube Sizes",
					location:"/settings/tubes",
					icon:"eyedropper",
				},
				{
					name: "Accounts",
					location:"/settings/users", 
					icon:"user-md", 
				}
			]
		},
	];