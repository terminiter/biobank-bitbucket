exports.normal = function(req, res, next) {
	var redirect = encodeURIComponent(req.url);
	if(req.session.user == null || req.session.user.name == null) 
	{
		res.locals.set_user(null);
		req.session.user = null;
		res.redirect('/login?redirect='+redirect);
		return;
	}
	else
	{
		res.locals.set_user(req.session.user);
	}
	next();
};

exports.admin = function(req, res, next) {
	exports.normal(req,res, function(){
		if(req.session.user && req.session.user.access != 0)
		{
			res.redirect('/no_belong');
		}
		next();
	});
};