exports.navbar = require('../library/navbar.js');
exports.user = null;
exports.remove_user = function()
{
	exports.user = null;
}
exports.set_user = function(user)
{
	exports.user = user;
}
exports.jsesc = require('jsesc');
exports.toUnicode = function(string)
{
	return string.split('').map(function(t) { return ('000' + t.charCodeAt(0).toString(16)).substr(-4) }).join('');
}
exports.dateChange = {
	goodstr: function(d)
	{
		return d.toLocaleDateString() + " " + d.toLocaleTimeString();
	},
	nicestr: function(d)
	{
		return d.toDateString();
	},
	dob: function(d)
	{
		changeStr = function(num)
		{	
			if(num < 10)
				return "0"+num;

			else
				return num;
		};

		return changeStr(d.getDate())+"-"+changeStr(d.getMonth())+"-"+changeStr(d.getFullYear());
	},
}
exports.deleteDate = function(){
	return new Date().toDateString().split(" ").slice(1).join("-");
}