var Donation = require('../models/functions/donation.js');
var async = require('async');

exports.setSamples = function()
{
	Donation.Model.Sample.find({}, function(err, samples) {
		Donation.Model.populate(samples, "donation", function(err, samples) {
			console.log("Found "+samples.length+" samples...");
			var already = 0;
			var fixed = 0;
			var not = 0;
			async.each(samples, function(sample, next) {
				if(sample.donation == null)
				{
					Donation.Model.findOne({"samples._id":sample._id}, function(err, donation){
						if(donation)
						{
							sample.donation = donation;
							sample.save(function(err, sample) {
								if(sample.donation != null)
								{
									fixed++
								}
								else
								{
									not++
								}
								next();
							})
						}
						else
						{
							not++;
							next();
						}
					})
				}
				else
				{
					already++;
					next();
				}
			}, function(err) {
				console.log("Looked at all samples, "+already+" already fixed & "+fixed+" fixed & "+not+" not fixed");
			})
		})
	})
}

exports.findSamples = function()
{
	Donation.Model.Sample.find({}, function(err, samples) {
		console.log(samples);
	})
}