module.exports = function(FunctionObject, model_name){
	return {
		create: function(req,res)
		{
			if(model_name != null)
				var modelData = req.body[model_name];
			else
				var modelData = req.body;

			FunctionObject.create(modelData, function(err, saved_model){
				if(err)
					res.send("Error");
				else
					res.send(saved_model);
			})
		},
		delete: function(req, res)
		{
			FunctionObject.delete(req.params.date, req.params.id, function(output){
				res.send(output);
			})
		},
		update: function(req,res)
		{
			if(model_name != null)
				var modelData = req.body[model_name];
			else
				var modelData = req.body;
			
			FunctionObject.Model.findById(req.params.id, function(err, model){
				if(model)
				{
					Freezer.update(model._id, modelData, function(err, saved_freezer) {
						if(err)
							res.send(err);
						else
							res.send(saved_freezer);
					})
				}
			});
			
		}
	}
}