module.exports = function(Model){
	return {
		getAll: function(callback)
		{
			Model.find({}, function(err, data){
				callback(data);
			})
		},
		create: function(data, callback)
		{

			var f = new Model(data);

			f.save(function(err) {
				if(err) console.log(err);
				
				Model.findById(f,callback);
			})
		},
		update: function(id, data, callback)
		{
			Model.findByIdAndUpdate(id, data, callback)
		},
		delete: function(date, id, callback)
		{
			var dateStr = new Date().toDateString().split(" ").slice(1).join("-");
			if(dateStr == date)
				Model.findById(id, function(err, object) {
					console.log(id);
					console.log(Model);
					Model.findByIdAndRemove(object._id, function(err) {
						if(err)
							callback(err);
						else
							callback("OK");
					})
				});
			else
				callback("Not Here");
		},
		Model: Model
	}
}