function printToPdf(){
    //Get the print button and put it into a variable
    
    var notShown = document.getElementsByClassName("dontPrint1"),
    i = notShown.length;
    //Set the print button visibility to 'hidden' 
    while(i--) {
        notShown[i].style.visibility = 'hidden';
    }
    //Print the page content
    window.print()
    //Set the print button to 'visible' again 
    //[Delete this line if you want it to stay hidden after printing]
    //notShown.style.visibility = 'visible';
    i = notShown.length;
    //Set the print button visibility to 'hidden' 
    while(i--) {
        notShown[i].style.visibility = 'visible';
    }
}
