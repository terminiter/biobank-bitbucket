bioApp.controller('donationSamplesView', function($scope, $http) {
	$scope.sampleTypes = samplesTypes;
	$scope.freezers = freezers;
	$scope.surveys = surveys;

	var getSample = function(id)
	{
		for(var i = 0; i<$scope.sampleTypes.length; i++)
		{
			if($scope.sampleTypes[i]._id == id)
				return i;
		}

		return null;
	}

	var getFreezer = function(id)
	{
		for(var i = 0; i<$scope.freezers.length; i++)
		{
			if($scope.freezers[i]._id == id)
				return i;
		}
		return null;
	}

	$scope.createModal = function()
	{
		$scope.sampleEdit = false;
		$scope.sampleId = null;
		$scope.sampleLabel = null;
		$scope.sampleType = $scope.sampleTypes[0];
		$scope.freezer = $scope.freezers[0];
		$("#sampleModal").modal('show');
	}

	$scope.editModal = function(id, label, st)
	{
		$scope.sampleEdit = true;
		$scope.sampleId = id;
		$scope.sampleLabel = label;
		$scope.sampleType = $scope.sampleTypes[getSample(st._id)];
		$("#sampleModal").modal('show');
	}


	$scope.onSubmit = function()
	{
		if(!$scope.loading)
		{
			$scope.loading = true;
			if($scope.freezer && $scope.freezer._id)
			{
				data = {type:$scope.sampleType._id, freezer: $scope.freezer._id};
			}
			else
			{
				data = {type:$scope.sampleType._id};
			}

			var url = '/sample/create/'+donationId+'/single';
			if($scope.sampleId != null && $scope.sampleEdit == true)
			{
				delete data.freezer;
				url = '/sample/update/'+donationId+'/'+$scope.sampleId;
			}
			$http.post(url,{sample:data})
			.success(function(returnData) {
				$scope.loading = false;
				if(returnData == "ok")
				{
					location.reload(true);
					console.log(returnData);
				}
				else
				{
					alert("Error");
					console.log(data);
					$scope.loading = false;
				}

			}).error(
				function(data){
					alert("Error");
					console.log(data);
					$scope.loading = false;

				}
			)
		}
	}
	$scope.deleteSample = function(id)
	{
		var d = new Date().toDateString().split(" ").slice(1).join("-");
		deleteConfirm("this sample", function(result) {
			if(result)
				$http.get('/sample/delete/'+donationId+'/'+id+'/delete/'+d).success(function(data) {
					location.reload(true);
				})
		})
	}

	$scope.deleteSurvey = function(index, id)
	{
		var d = new Date().toDateString().split(" ").slice(1).join("-");
		deleteConfirm("this survey", function(result) {
			if(result)
				$http.get('/donation/'+donationId+'/survey/remove/'+index+'/'+id+'/'+d).success(function(data) {
					location.reload(true);
				})
		})
	}
	$scope.deleteSurveyResult = function(survey_index, survey_id, result_index, result_id)
	{
		var d = new Date().toDateString().split(" ").slice(1).join("-");
		deleteConfirm("this survey infomation", function(result) {
			if(result)
				$http.get('/donation/'+donationId+'/survey/result/remove/'+survey_index+'/'+survey_id+'/'+result_index+'/'+result_id+'/'+d).success(function(data) {
					location.reload(true);
				})
		})
	}

	$scope.showResultModal = function(survey_index, survey_id, result_index, result_id)
	{
		$scope.result_survey = {index:survey_index,id :survey_id};

		if(result_id && result_index)
		{
			$http.get('/donation/'+donationId+'/survey/result/find/'+survey_index+'/'+survey_id+'/'+result_index+'/'+result_id)
			.success(function(data){
				var result = data.result;
				$scope.result = result;
				$scope.result.name = result.key;
				if(result.type == 'mc' || result.type == 'list')
				{
					$scope.result.type = "list";
					console.log(result);
					var arr = result.value.split("__");
					$scope.result.list = arr;
				}
				$scope.result_info = {id: result._id, index: data.index};
				$("#resultCreator").modal('show');
			})
		}
		else
		{
			$("#resultCreator").modal('show');
		}
	}

	$scope.addResult = function()
	{
		console.log($scope.result);
		console.log();
		var data = $scope.result

		if(data.type == "list")
			data.value = data.list;
		var url = '/donation/'+donationId+'/survey/result/add/'+$scope.result_survey.index+'/'+$scope.result_survey.id;
		if($scope.result_info && $scope.result_info.id)
		{
			url = url = '/donation/'+donationId+'/survey/result/update/'+$scope.result_survey.index+'/'+$scope.result_survey.id+'/'+$scope.result_info.index+'/'+$scope.result_info.id;
		}

		$http.post(url, {result:data})
		.success(function(data){
			location.reload(true);
		})
	}

	deleteConfirm = function(name, callback)
	{
		bootbox.confirm("Do you wish to delete "+name+"?", function(result) {
			if(result)
			{
				bootbox.confirm("Are you sure, THIS WILL NEVER BE UNDONE!", function(result) {
					callback(result)
				}); 
			}
			else
			{
				callback(false)
			}
			
		}); 
	}
});