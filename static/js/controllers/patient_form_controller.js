bioApp.controller('patientFormController', function($scope, $http) {
	changeDob = function(d)
	{
		console.log(d);
		$scope.patient.dob = changeStr(d.getDate())+"-"+changeStr(d.getMonth()+1)+"-"+changeStr(d.getFullYear());
	}
	changeStr = function(num)
	{	
		if(num < 10)
			return "0"+num;

		else
			return num;
	}

	if(local_patient)
	{

		$scope.patient = local_patient;
		$scope.loading = false;
		$scope.patientId = local_patient._id;
		$scope.checked = true;

		if(local_patient.dob)
			changeDob(new Date(local_patient.dob));
		else
			changeDob(new Date());
	}
	else
	{
		$scope.patient = {};
		$scope.loading = false;
		$scope.patientId = null;
		changeDob(new Date());
	}

	$scope.createPatient = function()
	{
		if(!$scope.loading)
		{
			$scope.loading = true;
			data = $scope.patient;
			var url = '/patient/create';
			if($scope.patientId != null)
				url = '/patient/'+$scope.patientId;

			$http.post(url,{patient:data})
			.success(function(returnData) {
				$scope.loading = false
				$scope.patientId = null;
				if(returnData != "Error")
				{
					$scope.patientId = returnData._id;
					location.replace('/patient/'+$scope.patientId);
				}

			}).error(
				function(data){
					alert("500 - Error");
					console.log(data);
					$scope.loading = false;

				}
			)
		}
	}
	$scope.init = function()
	{
		$('#datepicker').datepicker({"viewMode":2})
		.on('changeDate', function(ev){
			changeDob(ev.date);
			$scope.$apply()
		});
	}
	$scope.init();
	
});