bioApp.controller('samplelookupController', function($scope, $http) {
	$scope.question = {};
	$scope.question.querys = [];
	var key = function() {
		return {
			key: "",
			value: ""
		};
	}
	
	$scope.addKey = function()
	{
		$scope.question.querys.push(new key());
	}

	$scope.getQuery = function(key,value) {
		var url = "/api/search/query"

		if(value && key)
			url += "?key="+key+"&value="+value;
		else if(value && !key)
			url += "?value="+value;
		else if(key)
			url += "?key="+key;
		
		return $http.get(url).then(function(response){
			return response.data;
		});
	}

});