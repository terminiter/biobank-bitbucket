bioApp.controller('addDataController', function($scope, $http) {
	$scope.loading = false;
	var sampleId = local_donation._id;
	console.log(sampleId);

	$scope.newData = function(data, index)
	{
		$scope.fileId = null;
		$scope.file = {type:"",location:"", description:""}

		if(data != null)
		{
			$scope.fileId = null;
			$scope.fileId = data._id;
			$scope.fileIndex = index;
			$scope.file = {type:data.type, location:data.location, description:data.description}
		}

		$("#dataModal").modal('show');

		$scope.loading = false;
		$scope.update = (data != null);
	}

	$scope.editData = function(id, index, entryid)
	{
		var url = '/sample/'+id+'/dataentry/'+index+'/'+entryid;
		$http.get(url)
		.success(function(returnData){
			$scope.newData(returnData, index);
		})
	}

	$scope.createFile = function()
	{
		if(!$scope.loading)
		{
			$scope.loading = true;
			data = $scope.file;
			var url = '/sample/'+sampleId+'/dataentry/';
			
			if($scope.fileId != null && $scope.fileIndex != null)
				url = '/sample/'+sampleId+'/dataentry/'+$scope.fileIndex+'/'+$scope.fileId+'/update';

			$http.post(url,{dataentry:data})
			.success(function(returnData) {
				if(returnData != "Error")
				{
					location.reload(true);
				}
			}).error(
				function(data){
					alert("500 - Error");
					console.log(data);
					$scope.loading = false;

				}
			)
		}
	}
	$scope.deleteData = function(id, index)
	{
		var d = new Date().toDateString().split(" ").slice(1).join("-");
		deleteConfirm(name, function(result) {
			if(result)
				$http.get('/sample/'+id+'/dataentry/'+index+'/delete/'+d).success(function(data) {
					location.reload(true);
				})
		})
	}

	deleteConfirm = function(name, callback)
	{
		bootbox.confirm("Do you wish to delete this entry?", function(result) {
			if(result)
			{
				bootbox.confirm("Are you sure, THIS WILL NEVER BE UNDONE!", function(result) {
					callback(result)
				}); 
			}
			else
			{
				callback(false)
			}
			
		}); 
	}
});