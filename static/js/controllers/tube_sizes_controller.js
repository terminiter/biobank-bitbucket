bioApp.controller('tubeSizesController', function($scope, $http) {
	$scope.newTube = function(data)
	{
		$scope.tubeSizeId = null;
		$scope.tube = {name:"", description:""}

		if(data != null)
		{
			$scope.tubeSizeId = data._id;
			$scope.tube = {name:data.name, description:data.description}
		}

		
		$("#tubeModal").modal('show');

		$scope.loading = false;
		$scope.update = (data != null);
	}

	$scope.createTube = function()
	{
		if(!$scope.loading)
		{
			$scope.loading = true;
			data = $scope.tube;
			var url = '/settings/tube/create';
			
			if($scope.tubeSizeId != null)
				url = '/settings/tube/'+$scope.tubeSizeId;

			$http.post(url,{tube:data})
			.success(function(returnData) {
				if(returnData != "Error")
				{
					reload();
				}
			}).error(
				function(data){
					alert("500 - Error");
					console.log(data);
					$scope.loading = false;

				}
			)
		}
	}
	$scope.removeTube = function(tube)
	{
		var d = new Date().toDateString().split(" ").slice(1).join("-");
		console.log(tube);
		var name = tube.name;
		var id = tube._id;
		deleteConfirm(name, function(result) {
			if(result)
				$http.get('/settings/tube/'+id+'/delete/'+d).success(function(data) {
					reload();
				})
		})
	}

	deleteConfirm = function(name, callback)
	{
		bootbox.confirm("Do you wish to delete "+name+"?", function(result) {
			if(result)
			{
				bootbox.confirm("Are you sure, THIS WILL NEVER BE UNDONE!", function(result) {
					callback(result)
				}); 
			}
			else
			{
				callback(false)
			}
			
		}); 
	}

	reload = function()
	{
		$http.get('/settings/tubes/json').success(function(data){
			$scope.tube_sizes = data;
			$scope.loading = false;
			$("#tubeModal").modal('hide');
		})
	}
	$scope.reload = reload;
	$scope.tube_sizes = local_tube_sizes;
});