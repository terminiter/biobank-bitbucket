bioApp.controller('surveysViewController', function($scope, $http, $rootScope) {

	$scope.removeSurvey = function(nameenc, id)
	{
		var d = new Date().toDateString().split(" ").slice(1).join("-");
		var name = decodeURIComponent(nameenc);
		deleteConfirm(name, function(result) {
			if(result)
				$http.get('/settings/survey/'+id+'/delete/'+d).success(function(data) {
					location.reload(true);
				})
		})
	}

	deleteConfirm = function(name, callback)
	{
		bootbox.confirm("Do you wish to delete "+name+"?", function(result) {
			if(result)
			{
				bootbox.confirm("Are you sure, THIS WILL NEVER BE UNDONE!", function(result) {
					callback(result)
				}); 
			}
			else
			{
				callback(false)
			}
			
		}); 
	}
});