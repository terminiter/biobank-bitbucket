bioApp.controller('settingsUsersController', function($scope, $http) {
	$scope.newUser = function(data)
	{
		$scope.userID = null;
		$scope.password = "";
		$scope.password2 = "";

		if(data != null)
		{
			$scope.userID = data._id;
			$scope.user = data;
			console.log($scope.user);
		}

		$("#sampleModal").modal('show');

		$scope.loading = false;
		$scope.update = (data != null);
		$scope.update_password = ($scope.update) ? "No" : "Yes";
	}

	$scope.createUser = function()
	{
		if(!$scope.loading)
		{
			$scope.loading = true;
			data = $scope.user;
			var url = '/settings/user/create';
			
			if($scope.userID != null)
			{
				url = '/settings/user/'+$scope.userID;
			}

			if($scope.password != "" && $scope.password2 != $scope.password)
			{
				$scope.loading = false;
				return;
			}

			if($scope.password != "")
				data.password = $scope.password;

			$http.post(url,{user:data})
			.success(function(returnData) {
				if(returnData != "Error")
				{
					reload();
				}
			}).error(
				function(data){
					alert("500 - Error");
					console.log(data);
					$scope.loading = false;

				}
			)
		}
	}
	$scope.removeUser = function(user)
	{
		var d = new Date().toDateString().split(" ").slice(1).join("-");
		console.log(user);
		var name = user.name;
		var id = user._id;
		deleteConfirm(name, function(result) {
			if(result)
				$http.get('/settings/user/'+id+'/delete/'+d).success(function(data) {
					reload();
				})
		})
	}

	deleteConfirm = function(name, callback)
	{
		bootbox.confirm("Do you wish to delete "+name+"?", function(result) {
			if(result)
			{
				bootbox.confirm("Are you sure, THIS WILL NEVER BE UNDONE!", function(result) {
					callback(result)
				}); 
			}
			else
			{
				callback(false)
			}
			
		}); 
	}

	reload = function()
	{
		$http.get('/settings/users/json').success(function(data){
			$scope.users = data;
			$scope.loading = false;
			$("#sampleModal").modal('hide');
		})
	}
	$scope.reload = reload;
	$scope.users = local_users;
});