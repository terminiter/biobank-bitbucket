bioApp.controller('freezerController', function($scope, $http) {
	$scope.box_dimensions = [{name:"4x4",value:4},{name:"5x5",value:5},{name:"6x6",value:6},{name:"7x7",value:7},{name:"8x8",value:8},{name:"9x9",value:9},{name:"10x10",value:10}];
	if(local_freezer)
	{
		$scope.freezer = local_freezer;
		$scope.loading = false;
		$scope.freezerId= local_freezer._id;
		for(var i = 0; i < $scope.freezer.shelves.length; i++)
		{
			var val = $scope.freezer.shelves[i].box_dim.value;
			$scope.freezer.shelves[i].box_dim = $scope.box_dimensions[val-4];
		}
		$scope.$apply();
	}
	else
	{
		$scope.freezer = {};
		$scope.freezer.name = "";
		$scope.freezer.shelves = [];
	}
	var shelf = function() {
		return {
			num_racks: 6,
			num_drawers: 5,
			num_boxes: 5,
			box_dim: $scope.box_dimensions[5]
		};
	}
	
	
	$scope.addShelf = function()
	{
		$scope.freezer.shelves.push(new shelf());
	}
	$scope.getShelfTotal = function(index)
	{
		var shelf = $scope.freezer.shelves[index];
		return shelf.num_racks*shelf.num_drawers*shelf.num_boxes*(shelf.box_dim.value*shelf.box_dim.value);
	}
	$scope.getFreezerTotal = function()
	{
		var total = 0;
		for(var i = 0; i<$scope.freezer.shelves.length; i++)
			{
				total += $scope.getShelfTotal(i);
			}
		
		return total;
	}
	
	$scope.onSubmit = function()
	{
		if(!$scope.loading)
		{
			$scope.loading = true;
			data = $scope.freezer;
			var url = '/freezer/create';
			if($scope.freezerId != null)
				url = '/freezer/'+$scope.freezerId;

			$http.post(url,{freezer:data})
			.success(function(returnData) {
				$scope.loading = false
				$scope.freezerId = null;
				if(returnData != "Error")
				{
					$scope.freezerId = returnData._id;
					location.replace('/freezers');
				}

			}).error(
				function(data){
					alert("500 - Error");
					console.log(data);
					$scope.loading = false;

				}
			)
		}
	}

}
);