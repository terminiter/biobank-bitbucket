/* Initialize your widget via javascript as follows */
$("#input-20").fileinput({
	browseClass: "btn btn-primary",
	uploadClass: "btn btn-success",
	removeClass: "btn btn-danger",
	showCaption: false,
	showRemove: true,
	showUpload: true,
});