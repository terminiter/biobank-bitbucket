$(function() {
 Morris.Area({
        element: 'morris-area-chart',
        data: [{
            period: '2010 Q1',
            patients: 2666,
            surveys: null,
            samples: 2647
        }, {
            period: '2010 Q2',
            patients: 2778,
            surveys: 2294,
            samples: 2441
        }, {
            period: '2010 Q3',
            patients: 4912,
            surveys: 1969,
            samples: 2501
        }, {
            period: '2010 Q4',
            patients: 3767,
            surveys: 3597,
            itouch: 5689
        }, {
            period: '2011 Q1',
            patients: 6810,
            surveys: 1914,
            samples: 2293
        }, {
            period: '2011 Q2',
            patients: 5670,
            surveys: 4293,
            samples: 1881
        }, {
            period: '2011 Q3',
            patients: 4820,
            surveys: 3795,
            samples: 1588
        }, {
            period: '2011 Q4',
            patients: 15073,
            surveys: 5967,
            samples: 5175
        }, {
            period: '2012 Q1',
            patients: 10687,
            surveys: 4460,
            samples: 2028
        }, {
            period: '2012 Q2',
            patients: 8432,
            surveys: 5713,
            samples: 1791
        }],
        xkey: 'period',
        ykeys: ['patients', 'surveys', 'samples'],
        labels: ['patients', 'surveys', 'samples'],
        pointSize: 4,
        hideHover: 'auto',
        lineColors: ['#d9534f','#5cb85c','#428bca'],
        resize: true
    });

   
       
       
  
    Morris.Donut({
        element: 'morris-donut-chart',
        data: [{
            label: "Patients",
            value: 50
        }, {
            label: "Surveys",
            value: 70
        }, {
            label: "Samples",
            value: 560
        }],
        resize: true,
      colors: ['#d9534f','#5cb85c','#428bca']
    });

   

});
