// module to expose all routes
exports.api = require('./api.js');
exports.normal = require('./normal.js');
exports.sessions = require('./sessions.js');
exports.debug = require('./debug.js');


exports.redirect = function(req, res) {res.redirect('/')};