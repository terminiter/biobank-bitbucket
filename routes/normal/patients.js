var Patient = require("../../models/functions/patient.js");
var Donation = require("../../models/functions/donation.js");
var Survey = require("../../models/functions/survey.js");

exports.view = function(req,res)
{
	Patient.getAll(function(patients) {
		res.render('patients/patients_view', {patients: patients});
	})
	
}

exports.form = function(req,res)
{
	if(req.params.id)
		Patient.Model.findById(req.params.id, function(err, patient){
			if(patient)
			{
				res.render('patients/patients_form', {edit:true, patient:patient});
			}
			else
			{
				res.redirect("/patients")
			}
		})
	else
		res.render('patients/patients_form', {});
	
}

exports.details = function(req,res)
{
	Patient.Model.findById(req.params.id, function(err, patient){
		if(patient)
		{
			Survey.getAll(function(surveys){
				Donation.findByPatient(patient._id, function(err, donations){
					res.render('patients/patients_details', {patient:patient, donations:donations, surveys:surveys});
				});
			});
		}
		else
		{
			res.redirect('/patients');
		}
	})
}

exports.update = function(req,res)
{
	var patientData = req.body.patient;
	Patient.Model.findById(req.params.id, function(err, patient){
		if(patient)
		{
			Patient.update(patient._id, patientData, function(err, saved_patient) {
				if(err)
					res.send(err);
				else
					res.send(saved_patient);
			})
		}
	});
	
}
exports.create = function(req,res)
{
	var patient = req.body.patient;

	Patient.create(patient, function(err, saved_patient){
		if(err)
			res.send("Error");
		else
			res.send(saved_patient);
	})
}
exports.json = function(req,res)
{
	res.render('patients/patients_details', {});
	
}

exports.delete = function(req, res)
{
	Patient.delete(req.params.date, req.params.id, function(output){
		res.send(output);
	})
}

exports.image = {
	upload: function (req, res) {
		Patient.Model.findById(req.params.id, function(err, patient){
			if(patient)
			{
				if(req.files && req.files.pconsent)
				{
					Patient.Consent.upload(patient._id, req.files.pconsent, function(err, patient){
						if(patient)
						{
							res.redirect('/patient/'+patient._id);
						}
						else
						{
							res.send(err);
						}
					})
				}
				else
				{
					Patient.Consent.delete(patient._id, function(err, patient){
						if(patient)
						{
							res.redirect('/patient/'+patient._id);
						}
					})
				}
			}
			else
			{
				res.send("No Patient");
			}
		})
	},
	remove: function(req,res)
	{
		Patient.Model.findById(req.params.id, function(err, patient){
			if(patient)
			{
				Patient.Consent.delete(patient._id, function(err, patient){
					if(patient)
					{
						res.send("ok");
					}
					else
						res.send(err);
				})
			}
			else
			{
				res.send("No Patient");
			}
		});
	}
}

exports.donation = {
	view:function(req, res)
	{
		Donation.findByPatient(req.params.id, function(err, donations, patient) {
			if(donations && donations.length > 0)
				Donation.getMutiple(donations, function(donations) {
					res.render('patients/donation_delete_printout', {patient:patient, donations:donations})
				});
			else
				res.render('patients/donation_delete_printout', {patient:patient, donations:donations})
		})
	},
	delete:function(req, res)
	{
		Patient.Donation.delete(req.params.date, req.params.id, function(output){
			if(output == "OK")
				res.redirect('/patients');
			else
				res.send(output);
		})
	}
}
