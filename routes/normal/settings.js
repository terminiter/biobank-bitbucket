var Survey = require("../../models/functions/survey.js");
var SampleTypes = require("../../models/functions/sample_type.js");
var TubeSizes = require("../../models/functions/tube_size.js");
var Admin = require("../../models/functions/admin.js");

exports.view = function(req,res)
{
	res.render('settings', {});
}

exports.surveys =
{
	view: function(req,res)
	{
		Survey.getAll(function(surveys) {
			res.render('settings/surveys', {surveys: surveys});
		})
	},
	json: function(req, res)
	{
		Survey.view(function(surveys)
		{
			res.send(surveys);
		})
	}
}

exports.samples =
{
	view: function(req,res)
	{
		SampleTypes.getAll(function(sample_types) {
			res.render('settings/sample_types', {sample_types: sample_types});
		})
	},
	json: function(req,res)
	{

		SampleTypes.getAllWithNumbers(function(all){
			res.send(all);
		})
		
	}
}
//get all tubes pass to view and pass to json
exports.tubes =
{
	view: function(req,res)
	{
		TubeSizes.getAll(function(tube_sizes) {
			res.render('settings/tube_sizes', {tube_sizes: tube_sizes});
		})
	},
	json: function(req,res)
	{

		TubeSizes.getAll(function(all){
			res.send(all);
		})
		
	}
}

exports.users =
{
	view: function(req,res)
	{
		Admin.getAll(function(users) {
			res.render('settings/accounts', {users: users});
		})
	},
	json: function(req,res)
	{

		Admin.getAll(function(all){
			res.send(all);
		})
		
	}
}
exports.user = require('./settings_user.js');
exports.survey = require('./settings_survey.js');
exports.sample = require('./settings_sample.js');
exports.tube = require('./settings_tube.js');