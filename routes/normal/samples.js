var Donation = require("../../models/functions/donation.js");
var SampleType = require("../../models/functions/sample_type.js");
var Survey = require("../../models/functions/survey.js");
var model_routes = require("../../extensions/routing_functions.js")(Donation);
var Freezer = require("../../models/functions/freezer.js");

exports.view = function(req,res)
{
	Donation.getAll(function(donations) {
		res.render('donation/samples_view', {donations:donations});
	});
}
exports.dataentry = {
	view: function(req,res)
	{
		Donation.getOne(req.params.id, function(err, donation){
			res.render('sample/samples_dataentry', {donation:donation});
		});
	},
	update: function(req, res)
	{
		Donation.DataEntry.update(req.params.id, req.params.index, req.params.entryid, req.body.dataentry, function(err, donation){
			if(donation != null)
				res.send("ok")
			else
				res.send(err)
		})
	},
	delete: function(req, res)
	{
		Donation.DataEntry.delete(req.params.id, req.params.index, req.params.date, function(err, donation){
			if(donation != null)
				res.send("ok")
			else
				res.send(err)
		})
	},
	create: function(req, res)
	{
		Donation.DataEntry.create(req.params.id, req.body.dataentry, function(err, donation){
			if(donation != null)
				res.send("ok")
			else
				res.send(err)
		})
	},
	json: function(req, res)
	{
		Donation.DataEntry.get(req.params.id, req.params.index, req.params.entryid, function(err, donation){
			if(donation != null)
				res.send(donation)
			else
				res.send(err)
		})
	}
}

model_routes.details = function(req, res)
{
	if(req.params.id)
		Survey.getAllSmall(function(surveys) {
			Freezer.getAll(function(freezers) {
				SampleType.getAll(function(sampletypes) {
					Donation.getOne(req.params.id, function(err, donation){
						if(donation)
						{
							res.render('donation/donation_about', {donation:donation, sampletypes:sampletypes, freezers:freezers, surveys:surveys});
						}
						else
						{
							res.redirect("/samples")
						}
					})
				});
			});
		});
	else
		res.redirect("/sample");
}

model_routes.add = function(req, res)
{
	Donation.Model.findById(req.params.donationId, function(err, donation){
		if(donation && req.body.sample && req.body.sample.type)
		{
			Donation.addSamples(donation, [req.body.sample.type], req.body.sample.freezer, true, function(err, donation) {
				if(err) res.send("ERROR -"+err);
				else
					res.send("ok")
			})
		}
		else
		{
			res.redirect("/samples")
		}
	})
}
model_routes.update = function(req, res)
{
	Donation.Model.findById(req.params.donationId, function(err, donation){
		if(donation && req.body.sample && req.body.sample.type)
		{
			Donation.Sample.update(req.params.id, req.body.sample, donation._id, function(err, data) {
				console.log(data);
				if(err) res.send("ERROR - "+err);
				else
					res.send("ok")
			})
		}
		else
		{
			res.redirect("/samples")
		}
	})
}
model_routes.delete = function(req, res)
{
	Donation.Model.findById(req.params.donationId, function(err, donation){
		if(donation)
		{
			Donation.Sample.delete(req.params.id, donation._id, function(err, data) {
				if(err) res.send("ERROR - "+err);
				else
					res.send("ok")
			})
		}
		else
		{
			res.redirect("/samples")
		}
	})
}

exports.sample = model_routes;