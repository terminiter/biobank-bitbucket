var Survey = require('../../models/functions/survey.js');

exports.form = function(req,res)
{
	res.render('survey_form/survey_creator', {edit:false});
}
exports.update_form = function(req,res)
{
	Survey.Model.findById(req.params.id, function(err, survey){
		if(survey)
		{
			res.render('survey_form/survey_creator', {edit:true,survey:survey});
		}
		else
		{
			res.redirect('/settings/survey');
		}
	})
}

exports.create = function(req, res)
{
	var survey = req.body.survey;
	
	if(req.query.preview == "true")
		survey.status = "Incomplete";
	else
		survey.status = "Completed";

	Survey.create(survey, function(err, saved_survey){
		if(err)
			res.send("Error");
		else
			res.send(saved_survey);
	})

}
exports.update = function(req, res)
{
	var survey = req.body.survey;

	if(req.query.preview == "true")
		survey.status = "Incomplete";
	else
		survey.status = "Completed";

	Survey.update(req.params.id, survey, function(err, saved_survey){
		if(err)
			res.send("Error");
		else
			res.send(saved_survey);
	})
}
exports.delete = function(req, res)
{
	var dateStr = new Date().toDateString().split(" ").slice(1).join("-");
	
	if(dateStr == req.params.date)
		Survey.Model.findById(req.params.id, function(err, survey) {
			Survey.Model.findByIdAndRemove(survey._id, function(err) {
				if(err)
					res.send(err);
				else
					res.send("OK");
			})
		});
	else
		res.send("Not Here");
}
exports.preview = function(req, res)
{
	Survey.Model.findById(req.params.id, function(err, survey){
		if(survey)
		{
			console.log(survey);
			res.render('survey_form/preview', {survey: survey});
		}
		else
		{
			res.send("Err");
		}
	})
}