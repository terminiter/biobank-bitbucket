var Freezer = require("../../models/functions/freezer.js");
var SampleType = require("../../models/functions/sample_type.js");

var TubeSize = require("../../models/functions/tube_size.js");

exports.form = function(req,res)
{
	TubeSize.getAll(function(tubesizes) {
		if(req.params.id)
			Freezer.Model.findById(req.params.id, function(err, freezer){
				if(freezer)
				{
					res.render('freezer/freezer_form', {edit:true, freezer:freezer,tubesizes:tubesizes});
				}
				else
				{
					res.redirect("/freezers")
				}
			})
		else
			res.render('freezer/freezer_form', {tubesizes:tubesizes});
	});
	
}

exports.about = function(req,res)
{
	if(req.params.id)
		SampleType.getAll(function(sampletypes) {
			Freezer.Model.findById(req.params.id, function(err, freezer){
				if(freezer)
				{
					Freezer.Model.populate(freezer, 'shelves.tube_size', function(err, freezer)
					{
						console.log(freezer.shelves);
						res.render('freezer/freezer_about', {freezer:freezer, sampletypes:sampletypes});
					})
				}
				else
				{
					res.redirect("/freezers")
				}
			})
		});
	else
		res.redirect("/freezers");
}

exports.view = function(req,res)
{
	Freezer.getAll(function(freezers) {
		res.render('freezer/freezer_view', {freezers: freezers});
	})
}
exports.model = {
	create: function(req,res)
	{
		var freezer = req.body.freezer;

		Freezer.create(freezer, function(err, saved_patient){
			if(err)
				res.send("Error");
			else
				res.send(saved_patient);
		})
	},
	delete: function(req, res)
	{
		Freezer.delete(req.params.date, req.params.id, function(output){
			res.send(output);
		})
	},
	update: function(req,res)
	{
		var freezerData = req.body.freezer;
		Freezer.Model.findById(req.params.id, function(err, freezer){
			if(freezer)
			{
				Freezer.update(freezer._id, freezerData, function(err, saved_freezer) {
					if(err)
						res.send(err);
					else
						res.send(saved_freezer);
				})
			}
		});
		
	}
}

var check = function(vari, number)
{
	return (vari < number && vari >= 0);
}