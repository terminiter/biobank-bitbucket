var TubeSize = require("../../models/functions/tube_size.js");
exports.update = function(req,res)
{
	var tubeSizeData = req.body.tube;
	
	TubeSize.Model.findById(req.params.id, function(err, tubeSize){
		if(tubeSize)
		{
			TubeSize.update(tubeSize._id, tubeSizeData, function(err, saved_tubeSize) {
				if(err)
					res.send(err);
				else
					res.send(saved_tubeSize);
			})
		}
	});
	
}
exports.create = function(req,res)
{
	var tubeSize= req.body.tube;

	TubeSize.create(tubeSize, function(err, saved_tubeSize){
		if(err)
			res.send("Error");
		else
			res.send(saved_tubeSize);
	})
}

exports.delete = function(req, res)
{
	TubeSize.delete(req.params.date, req.params.id, function(output){
		res.send(output);
	})
}
