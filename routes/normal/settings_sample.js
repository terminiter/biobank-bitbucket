var SampleType = require("../../models/functions/sample_type.js");
exports.update = function(req,res)
{
	var sampleTypeData = req.body.sample;
	
	SampleType.Model.findById(req.params.id, function(err, sampleType){
		if(sampleType)
		{
			SampleType.update(sampleType._id, sampleTypeData, function(err, saved_sampleType) {
				if(err)
					res.send(err);
				else
					res.send(saved_sampleType);
			})
		}
	});
	
}
exports.create = function(req,res)
{
	var sampleType = req.body.sample;

	SampleType.create(sampleType, function(err, saved_sampleType){
		if(err)
			res.send("Error");
		else
			res.send(saved_sampleType);
	})
}

exports.delete = function(req, res)
{
	SampleType.delete(req.params.date, req.params.id, function(output){
		res.send(output);
	})
}
