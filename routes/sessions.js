var Admin = require('../models/functions/admin.js');
var bcrypt     = require('bcrypt');

// GET /login
// Present the login page
exports.view = function(req, res) {
  var redirect = req.query.redirect ? decodeURIComponent(req.query.redirect) : "/";
  if (req.session.user) return res.redirect(redirect);

  var message = null;

  if(req.query.s)
  {
  	if(req.query.s == "0")
  	{
  		message = {message: "Username & Password can not be blank!", status: "warning"};
  	}
  	else if(req.query.s == "1")
  	{
  		message = {message: "Username & Password are invalid!", status: "danger"};
  	}
  	else
  	{
  		message = {message: "Logged Out Successfully!", status: "info"};
  	}
  }

  res.render('login', {message:message, redirect:encodeURIComponent(redirect)});
};

// GET /logout
// Destroys the session and redirects to the login page
exports.destroy = function(req, res) {
	req.session.user = null;
  res.locals.remove_user();
	res.redirect('/login?s=3');
};

// POST /login
// Attempt to authenticate the user and create
// a user session (cookie)
exports.create = function(req, res) {
  var redirect = req.query.redirect ? "&request="+req.query.redirect : "";
  var redirect_url = req.query.redirect ? decodeURIComponent(req.query.redirect) : "/";
	if(!req.body.username || !req.body.password)
	{
    res.redirect('/login?s=0'+redirect);
    return;
  }

	var username = req.body.username;
	var password = req.body.password;
	Admin.login(username, password, function(success, data){
		if(success)
		{
			req.session.user = data;
      res.locals.set_user(data);
			res.redirect(redirect_url);
		}
		else
		{
			res.redirect('/login?s=1'+redirect)
		}
	});
};


