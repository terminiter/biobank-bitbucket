var Donation = require("../../models/functions/donation.js");


exports.sample = function (req, res) {
	Donation.Model.findOne({"samples._id":req.params.sample_id})
	.exec(function(err, donation){
		if(err) donation = {};

		res.send(donation);
	})
}