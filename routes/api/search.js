var Donation = require("../../models/functions/donation.js");

exports.query = function(req, res)
{
	if(req.query.key)
	{
		if(req.query.value && req.query.key)
		{
			Donation.Search.keyValue(req.query.key,req.query.value,function(err, results){
				res.send(results);
			})
		}
		else if(req.query.key)
		{
			Donation.Search.key(req.query.key,function(err, results){
				res.send(results);
			})
		}
		else
		{
			Donation.Search.value(req.query.value,function(err, results){
				res.send(results);
			})
		}
	}
}

exports.label = function(req, res)
{

}

exports.find = function(req, res)
{
	Donation.Search.find(req.body.query, function(err, results){
		res.send(results);
	})
}