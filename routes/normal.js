exports.nobelong = function(req,res)
{
	res.render('nobelong', {});
}

exports.dashboard = require("./normal/dashboard.js");
exports.patients = require("./normal/patients.js");
exports.settings = require("./normal/settings.js");
exports.freezer = require("./normal/freezer.js");
exports.sample_lookup = require("./normal/sample_lookup.js");
exports.samples = require("./normal/samples.js");
exports.donation = require("./normal/donation.js");

exports.sample_print = require("./normal/sample_print.js");